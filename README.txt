
-- SUMMARY --

BetterTip (tooltip)is a handsome, modern gentleman that appears when
it�s showtime. It is Great for any kind of website and very practical.
This module is simply replace cool tooltip to all across the site that
tags have "title" attribute.

-- REQUIREMENTS --

None


-- INSTALLATION --

1. Copy the bettertip directory to your sites/SITENAME/modules directory.

2. Enable the module at Administer >> Modules.


-- CONFIGURATION --

1. Configuration > User-Interface > BetterTip
